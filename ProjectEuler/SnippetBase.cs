﻿namespace ProjectEuler
{
    /// <summary>The snippet base.</summary>
    public abstract class SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>The run.</summary>
        public virtual void Run()
        {
        }

        #endregion
    }
}