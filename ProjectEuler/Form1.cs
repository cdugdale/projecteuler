﻿namespace ProjectEuler
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;

    /*TODO
     * Add ability to drop class in* 
     * 
     */

    /// <summary>
    ///   The form 1.
    /// </summary>
    public partial class Form1 : Form
    {
        #region Fields

        /// <summary>
        ///   The _writer.
        /// </summary>
        private readonly TextWriter _writer;

        /// <summary>
        ///   The files list.
        /// </summary>
        private readonly string[] filesList;

        /// <summary>
        ///   The files list types.
        /// </summary>
        private readonly ArrayList filesListTypes;

        /// <summary>
        ///   The file path base.
        /// </summary>
        private string filePathBase =
            "C:\\Users\\Dugzor\\Dropbox\\Coding Workspace\\My Projects\\ProjectEuler\\ProjectEuler\\Snippets";

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initializes a new instance of the <see cref="Form1" /> class.
        /// </summary>
        public Form1()
        {
            this.InitializeComponent();

            this.filesList = Directory.GetFiles(this.filePathBase);

            this.AddItemsToDropdown();
            this.lblCalculatedTime.Text = string.Empty;

            this._writer = new TextBoxStreamWriter(this.txtOutput);
            Console.SetOut(this._writer);
            Console.Write("Please choose a snippet to run from the list above");
        }

        #endregion

        #region Methods

        /// <summary>
        ///   The add items to dropdown.
        /// </summary>
        private void AddItemsToDropdown()
        {
            foreach (var f in this.filesList)
            {
                
                this.ddlCodeChooser.Items.Add(Path.GetFileNameWithoutExtension(f));
            }
        }

        /// <summary>The file finder.</summary>
        /// <param name="fileName">The file name.</param>
        private void FileFinder(string fileName)
        {
          
            var obj = Activator.CreateInstance("ProjectEuler", "ProjectEuler.Snippets." + fileName);
            SnippetBase sb = (SnippetBase)obj.Unwrap();

            // use polymorphism to get the child to run :)
            sb.Run();
          
        }

        /// <summary>The snippet runner delegate.</summary>
        /// <param name="selectedItem">The selected Item. </param>
        private void SnippetChooserDelegate(string selectedItem)
        {
            Stopwatch snippetTimer = new Stopwatch();
            snippetTimer.Start();

            this.FileFinder(selectedItem);

            snippetTimer.Stop();
            this.lblCalculatedTime.Text = string.Format("Calculated in {0} ms", snippetTimer.ElapsedMilliseconds);
        }

        /// <summary>The combo box 1_ selected index changed.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void ddlCodeChooser_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtOutput.Text = string.Empty;
            this.txtSource.Text = string.Empty;
            this.SnippetChooserDelegate(this.ddlCodeChooser.SelectedItem.ToString());
            this.txtSource.Text =
                FileOperations.FileRead(this.filePathBase + "\\" + this.ddlCodeChooser.SelectedItem + ".cs");
        }

        /// <summary>The exit tool strip menu item_ click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>The list tool strip menu item_ click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormList list = new FormList();
            list.Show();
        }

        #endregion
    }
}