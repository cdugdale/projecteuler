﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace ProjectEuler
{
    public partial class FormList : Form
    {
        private string filePathBase =
            "C:\\Users\\Dugzor\\Dropbox\\Coding Workspace\\My Projects\\ProjectEuler\\ProjectEuler\\Snippets";


        public FormList()
        {
            InitializeComponent();

            var filesList = Directory.GetFiles(this.filePathBase);

            foreach (var f in filesList)
            {
                string filename = Path.GetFileNameWithoutExtension(f);
                this.textBox1.Text += (filename.Insert(5, ": ") + Environment.NewLine);
            }
        }
    }
}
