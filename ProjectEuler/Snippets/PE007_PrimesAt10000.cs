namespace ProjectEuler.Snippets
{
    using System;

    internal class PE007_PrimesAt10000 : SnippetBase
    {
        /// <summary>
        ///   The p e 007 primes at 10000.
        /// </summary>
        public override void Run()
        {
            // Write prime numbers between 0 and 100.
            //Console.WriteLine("--- Primes between 0 and 100 ---");
            //for (int i = 0; i < 100; i++)
            //{
            //    bool prime = HelperMethods.IsPrime(i); 
            //    if (prime)
            //    {
            //        Console.Write("Prime: ");
            //        Console.WriteLine(i);
            //    }
            //}

            // Write prime numbers between 10000 and 10100
            Console.WriteLine("--- Primes between 10000 and 10100 ---");
            for (int i = 10000; i < 10100; i++)
            {
                if (HelperMethods.IsPrime(i))
                {
                    Console.Write("Prime: ");
                    Console.WriteLine(i);
                }
            }
        }
    }
}