namespace ProjectEuler.Snippets
{
    using System;
    using System.Linq;
    using System.Numerics;


    internal class PE016_SumOfPower : SnippetBase
    {
        /// <summary>The p e 016 sum.</summary>
        public override void Run()
        {
            /*Problem16
2 15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
What is the sum of the digits of the number 2 1000 ?*/
            /*ulong baseNo = 2;
  ulong power = 50;*/



            string totalNumber = HelperMethods.CalculatePower(2, 1000).ToString();
            //System.Console.WriteLine(totalNumber);
            
            var intlist = totalNumber.Select(digit => int.Parse(digit.ToString()));

            int total = 0;
            foreach (int i in intlist)
            {
                total += i;
            }
            Console.Write(total);
        }

/* Another method
string a = System.Numerics.BigInteger.Pow(2,1000).ToString();

var b = a.Select(digit => int.Parse(digit.ToString()));

int sum = 0;

foreach (int c in b)
{
      sum += c;
}

Console.WriteLine(sum);
Console.ReadKey();*/


    }
}