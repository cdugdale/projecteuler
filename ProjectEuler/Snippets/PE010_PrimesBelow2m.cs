﻿namespace ProjectEuler.Snippets
{
    using System;


    //The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
    //Find the sum of all the primes below two million.

    /// <summary>The p e 010 primes below 2 m.</summary>
    internal class PE010_PrimesBelow2m : SnippetBase
    {
        /// <summary>The run.</summary>
        public override void Run()
        {
            int sum = 0;
            for (int i = 0; i <= 2000000; i++)
            {
                if (HelperMethods.IsPrime(i))
                {
                    sum += i;
                }
                
            }

            Console.WriteLine(sum);
        }
    }
}