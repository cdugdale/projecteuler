namespace ProjectEuler.Snippets
{
    using System;
    using System.Linq;
    using System.Numerics;

    internal class PE020_SumFactoral100 : SnippetBase
    {
        public override void Run()
        {
            /*n! means n (n 1) ... 3 2 1
            For example, 10! = 10 9 ... 3 2 1 = 3628800, and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
            Find the sum of the digits in the number 100!*/

            BigInteger starter = 100, sum = 1, facSum = 0;

            for (BigInteger i = 1; i <= starter; i++)
            {
                sum *= i;
                //System.Console.WriteLine(sum.ToString());
            }

            string sumString = sum.ToString();
            var intlist = sumString.Select(digit => int.Parse(digit.ToString()));

            int total = 0;
            foreach (int i in intlist)
            {
                total += i;
            }
            Console.Write(total);


            //BigInteger[] outarray = System.Array.ConvertAll(sum.ToString().ToArray(), x => (BigInteger)x - 48);

            //foreach(BigInteger d in outarray){System.Console.WriteLine(d);facSum += facSum;}
            ////for (BigInteger i = 0; i < outarray.Length; i++)
            ////{
            ////    BigInteger temp = outarray[(BigInteger)];
            ////    facSum += temp;
            ////}

            //System.Console.WriteLine("/nSum = " + facSum);
        }
    }
}