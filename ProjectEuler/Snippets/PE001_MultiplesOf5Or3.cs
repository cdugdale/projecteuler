namespace ProjectEuler.Snippets
{
    internal class PE001_MultiplesOf5Or3 : SnippetBase
    {
        /// <summary>
        ///   The p e 001 multiples of 5 or 3.
        /// </summary>
        public override void Run()
        {
            int sum = 0;

            for (int i = 0; i < 1000; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum += i;
                }
            }

            System.Console.WriteLine(sum);
        }
    }
}