namespace ProjectEuler.Snippets
{
    using System;
    using System.Diagnostics;

    /// <summary>The p e 005 divide by 20.</summary>
    internal class PE005_DivideBy20 : SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>
        ///   The p e 005 divide by 20.
        /// </summary>
        public override void Run()
        {
            /*
            //counting up from low to high with each loop took too long
            int p = 0, i;
            Stopwatch loopTimer = new Stopwatch();
            loopTimer.Start();

            do
            {
                p +=20;

                for (i = 1; i <= 20; i++)
                {
                    if (!(p % i == 0)) break;
                }
            } while (i <= 20);

            loopTimer.Stop();
            System.Console.WriteLine("Result: " + p + " calculated in "
            + loopTimer.ElapsedMilliseconds + "ms. Counting up from 1 to 20");
            */
            // counting down from high to low with each iteration. 
            int q = 0, j;
            Stopwatch loopTimer2 = new Stopwatch();
            loopTimer2.Start();

            do
            {
                q += 20;

                for (j = 20; j > 1; j--)
                {
                    if (!(q % j == 0))
                    {
                        break;
                    }
                }
            }
            while (j > 1);

            loopTimer2.Stop();
            Console.WriteLine("Result: " + q);

            // System.Console.WriteLine("Result: " + q + " calculated in " + loopTimer2.ElapsedMilliseconds + "ms.");
        }

        #endregion
    }
}