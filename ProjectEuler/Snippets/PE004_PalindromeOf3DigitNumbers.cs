﻿/* A palindromic number reads the same both ways. The largest palindrome made from the 
product of two 2-digit numbers is 9009 = 91  99.
Find the largest palindrome made from the product of two 3-digit numbers.
*/
namespace ProjectEuler.Snippets
{
    /// <summary>The p e 004 palindrome of 3 digit numbers.</summary>
    internal class PE004_PalindromeOf3DigitNumbers : SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>The run.</summary>
        public override void Run()
        {
            int firstNo, scndNo, sum1, sum2, revSum, largest = 0;

            for (firstNo = 500; firstNo <= 999; firstNo++)
            {
                for (scndNo = 500; scndNo <= 999; scndNo++)
                {
                    sum1 = firstNo * scndNo;
                    sum2 = sum1;

                    revSum = 0;
                    while (sum2 > 0)
                    {
                        revSum = revSum * 10 + sum2 % 10;
                        sum2 /= 10;
                    }

                    if (sum1 == revSum)
                    {
                        largest = revSum > largest ? revSum : largest;
                    }
                }
            }

            System.Console.WriteLine(largest);
        }

        #endregion
    }
}