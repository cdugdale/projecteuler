﻿/*A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, 
 the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n .

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers 
is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. 
However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be 
expressed as the sum of two abundant numbers is less than this limit.
Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
*/

namespace ProjectEuler.Snippets
{
    using System;
    using System.Collections.Generic;

    /// <summary>The p e 023 abundant numbers.</summary>
    internal class PE023_AbundantNumbers : SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>The run.</summary>
        public override void Run()
        {
            Console.WriteLine("Work In Progress");


                double a, divisionResult, sumDA;
            int sumDB, i, sumAmicables = 0;
            var addedNumbers = new List<double>();

            for (a = 1; a < 10000; a++)
            {
                if (addedNumbers.Contains(a) == false)
                {
                    sumDA = 0;
                    sumDB = 0;

                    for (i = 1; i < a; i++)
                    {
                        divisionResult = a / i;

                        if (divisionResult == Convert.ToInt32(divisionResult))
                        {
                            sumDA += i;
                        }
                    }

                    // check the division sum of the sum of a
                    for (int j = 1; j < sumDA; j++)
                    {
                        divisionResult = sumDA / j;

                        if (divisionResult == Convert.ToInt32(divisionResult))
                        {
                            sumDB += j;
                        }
                    }

                    if (a == sumDB && sumDA != sumDB)
                    {
                        addedNumbers.Add(sumDA);
                        sumAmicables += Convert.ToInt32(sumDA);
                        sumAmicables += sumDB;

                        // Details of A
                        // Console.WriteLine("d(" + a + ") = " + sumDA);

                        // Details of B
                        Console.WriteLine("d(" + a + ") = " + sumDA + "  ==  d(" + sumDA + ") = " + sumDB);
                        // Console.WriteLine("The sum of the amicable numbers numbers is: " + sumAmicables + "\n");
                    }
                }
            } Console.WriteLine("The sum of the amicable numbers numbers is: " + sumAmicables + "\n");
        }

        #endregion
    }
}