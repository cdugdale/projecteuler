namespace ProjectEuler.Snippets
{
    using System;

    /// <summary>
    ///   The p e 009 pythagoras.
    /// </summary>
    internal class PE009_Pythagoras : SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>
        ///   The p e 009 pythagoras.
        /// </summary>
        public override void Run()
        {
            /*A Pythagorean triplet is a set of three natural numbers, a b c , for which, a 2 + b 2 = c 2
            For example, 3 2 + 4 2 = 9 + 16 = 25 = 5 2 .
            There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc
            ratio: 3+4+5 = 12 upl test 2 */
            bool isTriplet = false;
            int a, b, c, abc, sumabc;

            for (a = 0; a < 250; a++)
            {
                for (b = 0; b < 400; b++)
                {
                    for (c = 0; c < 430; c++)
                    {
                        if (((a * a) + (b * b)) == (c * c))
                        {
                            if (c > b && b > a)
                            {
                                isTriplet = true;

                                sumabc = a + b + c;
                                if (sumabc == 1000)
                                {
                                    Console.WriteLine(a + "*" + b + "*" + c + "=" + sumabc + ".  ");
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}