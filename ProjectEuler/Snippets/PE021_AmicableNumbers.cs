﻿/* 
Let d( n ) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n). 
If d( a ) = b and d( b ) = a , where a ≠ b , then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. 
The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbersunder 10000.
*/
namespace ProjectEuler.Snippets
{
    using System;
    using System.Collections.Generic;

    /// <summary> The p e 021 amicable numbers. </summary>
    internal class PE021_AmicableNumbers : SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>The run.</summary>
        public override void Run()
        {
            double a, divisionResult, sumDA;
            int sumDB, i, sumAmicables = 0;
            var addedNumbers = new List<double>();

            for (a = 1; a < 10000; a++)
            {
                if (addedNumbers.Contains(a) == false)
                {
                    sumDA = 0;
                    sumDB = 0;

                    for (i = 1; i < a; i++)
                    {
                        divisionResult = a / i;

                        if (divisionResult == Convert.ToInt32(divisionResult))
                        {
                            sumDA += i;
                        }
                    }

                    // check the division sum of the sum of a
                    for (int j = 1; j < sumDA; j++)
                    {
                        divisionResult = sumDA / j;

                        if (divisionResult == Convert.ToInt32(divisionResult))
                        {
                            sumDB += j;
                        }
                    }

                    if (a == sumDB && sumDA != sumDB)
                    {
                        addedNumbers.Add(sumDA);
                        sumAmicables += Convert.ToInt32(sumDA);
                        sumAmicables += sumDB;
                       
                        Console.WriteLine("d(" + a + ") = " + sumDA + "  ==  d(" + sumDA + ") = " + sumDB);
                    }
                }
            } Console.WriteLine("The sum of the amicable numbers is: " + sumAmicables + "\n");
        }

        #endregion
    }
}