/* The prime factors of 13195 are 5, 7, 13 and 29.
 What is the largest prime factor of the number 600851475143 
*/
namespace ProjectEuler.Snippets
{
    using System;

    /// <summary>
    ///   The p e 003 primes.
    /// </summary>
    internal class PE003_Primes : SnippetBase
    {
        #region Public Methods and Operators

        /// <summary>
        ///   The p e 003 primes.
        /// </summary>
        public override void Run()
        {
            
            long largest = 1;
            bool isAPrime = false;
            long totalToTest = 600851475143;
            double totalToTestsq = System.Math.Sqrt(totalToTest);
            long totalToLoopTo = Convert.ToInt64(totalToTestsq);
            Console.WriteLine(totalToLoopTo);

            for (long i = 1; i <= totalToLoopTo; i += 2)
            {
                if (HelperMethods.IsPrime(i))
                {
                    if (totalToTest % i == 0)
                    {
                        // largest = largest < i ? i : largest;
                        largest = i;
                        Console.WriteLine(largest);

                        // BigInteger Result = totalToTest / i;
                    }
                }
            }

            Console.WriteLine("largest = " + largest);

            // for (long i = 1; i <= totalToTest; i += 2)
            // {

            // if (HelperMethods.IsPrime(i))
            // {
            // if (totalToTest % i == 0)
            // {
            // // largest = largest < i ? i : largest;
            // largest = i;
            // Console.WriteLine(largest);

            // // BigInteger Result = totalToTest / i;
            // }
            // }
            // }

            // Console.WriteLine("largest = " + largest);
        }

        #endregion
    }
}