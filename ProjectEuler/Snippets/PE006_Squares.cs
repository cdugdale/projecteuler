namespace ProjectEuler.Snippets
{
    using System;

    internal class PE006_Squares : SnippetBase
    {
        /// <summary>
        ///   The p e 006 squares.
        /// </summary>
        public override void Run()
        {
            int sum = 0, squaresSum = 0, difference = 0;

            for (int i = 0; i <= 100; i++)
            {
                squaresSum += i * i;
                sum += i;
            }

            difference = (sum * sum) - squaresSum;
            Console.Write(difference);
        }
    }
}