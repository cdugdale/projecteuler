﻿namespace ProjectEuler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
   
    using System.Windows.Forms;

    /// <summary>The file operations helper class.</summary>
    public static class FileOperations
    {
        #region Public Methods and Operators

        /// <summary>The file delete.</summary>
        /// <param name="fileToDelete">The file to delete </param>
        //public static void FileDelete(string fileToDelete)
        //{
        //    try
        //    {
        //        if (MessageBox.Show(
        //            "Are you sure you want to permanently delete the file\n" + fileToDelete,
        //            "Delete File?",
        //            MessageBoxButtons.YesNo) == DialogResult.Yes)
        //        {
        //            if (File.Exists(fileToDelete))
        //            {
        //                File.Delete(fileToDelete);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}

        /// <summary>The file read.</summary>
        /// <param name="filePath">The file path. </param>
        /// <returns>The <see cref="string"/> . </returns>
        public static string FileRead(string filePath)
        {
            try
            {
                string fname = filePath;

                var sr = new StreamReader(fname);
                string readcontent = sr.ReadToEnd();
                sr.Close();

                return readcontent;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return string.Empty;
        }

        /// <summary>The file write.</summary>
        /// <param name="pathToSaveTo">The path to save to. </param>
        /// <param name="textToSave">The text to save. </param>
        public static void FileWrite(string pathToSaveTo, string textToSave)
        {
            var file1 = new FileInfo(pathToSaveTo);

            StreamWriter sw = file1.CreateText();
            sw.Write(textToSave); // Writing content read from the textbox in the form	
            sw.Close();
        }

        /// <summary>The fill child nodes.</summary>
        /// <param name="node">The node. </param>
        //public static void FillChildNodes(TreeNode node)
        //{
        //    try
        //    {
        //        var dirs = new DirectoryInfo(node.FullPath);
        //        if (Settings.Default.ShowHIddenFilesAndFoldersSetting.Equals(true))
        //        {
        //            foreach (DirectoryInfo dir in dirs.GetDirectories())
        //            {
        //                var newnode = new TreeNode(dir.Name);
        //                node.Nodes.Add(newnode);
        //                newnode.Nodes.Add("*");
        //            }
        //        }
        //        else
        //        {
        //            foreach (DirectoryInfo dir in dirs.GetDirectories())
        //            {
        //                if ((dir.Attributes & FileAttributes.Hidden) == 0)
        //                {
        //                    var newnode = new TreeNode(dir.Name);
        //                    node.Nodes.Add(newnode);
        //                    newnode.Nodes.Add("*");
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        /// <summary>The method creates an IEnumerable generic of file path strings.</summary>
        /// <param name="path">The path. </param>
        /// <returns>The <see cref="string"/> . </returns>
        public static IEnumerable<string> GetFiles(string path)
        {
            string[] files = Directory.GetFiles(path);
            return files;
        }

        /// <summary>The save file as.</summary>
        /// <param name="textToSave">The text to save. </param>
        /// <param name="selectedDir">The selected directory. </param>
        /// <returns>The <see cref="string"/> . </returns>
        public static string SaveFileAs(string textToSave, string selectedDir)
        {
            string savedFileName;

            using (var dialog = new SaveFileDialog())
            {
                dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dialog.AddExtension = true;
                dialog.FilterIndex = 1;
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = selectedDir;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    FileWrite(dialog.FileName, textToSave);
                }

                savedFileName = dialog.FileName;
            }

            return savedFileName;
        }

        #endregion
    }
}