namespace ProjectEuler
{
    using System.Numerics;
    internal class HelperMethods
    {
        /// <summary>The calculate.</summary>
        /// <param name="???">The ???.</param>
        /// <param name="pow">The pow.</param>
        /// <returns>The <see cref="ulong"/>.</returns>
        public static BigInteger CalculatePower(BigInteger baseNumber, BigInteger pow)
        {
            BigInteger baseNo = baseNumber;
            BigInteger power = pow;
            BigInteger result = 1;
            for (BigInteger j = 1; j <= power; j++)
            {
                result *= baseNo;
            }

            return result;
        }

        /// <summary>The is prime.</summary>
        /// <param name="candidate">The candidate. </param>
        /// <returns>The <see cref="bool"/> . </returns>
        public static bool IsPrime(long candidate)
        {
            // Test whether the parameter is a prime number.
            if ((candidate & 1) == 0)
            {
                if (candidate == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // Note:
            // ... This version was changed to test the square.
            // ... Original version tested against the square root.
            // ... Also we exclude 1 at the very end.
            for (long i = 3; (i * i) <= candidate; i += 2)
            {
                if ((candidate % i) == 0)
                {
                    return false;
                }
            }

            return candidate != 1;

            // }
        }

        public static bool IsPrime(BigInteger candidate)
        {
            // Test whether the parameter is a prime number.
            if ((candidate & 1) == 0)
            {
                if (candidate == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // Note:
            // ... This version was changed to test the square.
            // ... Original version tested against the square root.
            // ... Also we exclude 1 at the very end.
            for (BigInteger i = 3; (i * i) <= candidate; i += 2)
            {
                if ((candidate % i) == 0)
                {
                    return false;
                }
            }

            return candidate != 1;

            // }
        }
    }
}